package itml.agents;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.List;
import java.util.Random;

import itml.cards.Card;
import itml.cards.CardRest;
import itml.common.Classifiers;
import itml.common.Filters;
import itml.common.MonteCarloResults;
import itml.simulator.CardDeck;
import itml.simulator.StateAgent;
import itml.simulator.StateBattle;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.Utils;
import weka.filters.Filter;



public class AgentBenDover extends Agent {

    private int TREE_DEPTH = 2;

	private int m_noThisAgent;     // Index of our agent (0 or 1).
    private int m_noOpponentAgent; // Index of opponent's agent.
    private HashMap<String, Card> cardsMap = new HashMap<String, Card>();
    
    public int correctGuesses;
    public int incorrectGuesses ;
    
    private Classifier m_Classifier = null;
    private Filter m_Filter = null;
    private Instances trainingData = null;
    private Card cardPredicted = null;

	public AgentBenDover(CardDeck deck, int msConstruct, int msPerMove, int msLearn) {
		super(deck, msConstruct, msPerMove, msLearn);
		try{
			// Populate hashmap with card names and class types so we 
			// can look them up later.
	        for (Card card : deck.getCards()) { 
	            cardsMap.put(card.getName(), card);
	        }
	        m_Classifier = Classifiers.J48();
	        m_Filter = Filters.RandomizeFilter();

            correctGuesses = 0;
            incorrectGuesses = 0;
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}


    public float predictionRatio() {
        return (float)correctGuesses / ((float)correctGuesses + (float)incorrectGuesses);
    }

	public void startGame(int noThisAgent, StateBattle stateBattle) {
		// Remember the indicies of the agents in the StateBattle.
        m_noThisAgent = noThisAgent;
        m_noOpponentAgent  = (noThisAgent == 0 ) ? 1 : 0; // can assume only 2 agents battling.

	}

	public void endGame(StateBattle stateBattle, double[] results) {
		//System.out.println("Correct predictions: " + correctGuesses + " Incorrect predictions: " + incorrectGuesses);
	}

	public Card act(StateBattle stateBattle) {
		
		//Fetch the state of the agents (stamina, location, health etc..)
		StateAgent asThis = stateBattle.getAgentState( m_noThisAgent );
        StateAgent asOpp  = stateBattle.getAgentState( m_noOpponentAgent );
		Card[] lastMoves = stateBattle.getLastMoves();
		
		if(lastMoves[0] != null || lastMoves[1] != null)
		{
			if (lastMoves[m_noOpponentAgent] == cardPredicted) {
				correctGuesses++;
			    //System.out.print("(Last prediction was correct. Predicted: " + cardPredicted.toString() + " Actual: " + lastMoves[m_noOpponentAgent].toString() + ") \n");
			} else {
				//System.out.print("(Last prediction was incorrect. Predicted: " + cardPredicted.toString() + " Actual: " + lastMoves[m_noOpponentAgent].toString() + ") \n");
				incorrectGuesses++;
			}
		}

        //Create an instance using the current state of the board.
        Instance currStateInstance = createInstanceToPredict(asThis, asOpp);

        try{
            double prediction = m_Classifier.classifyInstance(currStateInstance);
            String cardName = trainingData.classAttribute().value((int) prediction);
			cardPredicted = cardsMap.get(cardName);
		}
		catch(Exception e)
		{
			System.out.println("Classify error: " + e.getMessage());
		}

        //Populate the card array with cards we have enough stamina to use.
        ArrayList<Card> cards = m_deck.getCards( asThis.getStaminaPoints() );
        //Card foo =  cards.get(new Random().nextInt(cards.size()) );
        //return foo;
        return getNextMove(stateBattle, cards, cardPredicted);
	}

	public Classifier learn(Instances instances) {
		instances.setClassIndex(instances.numAttributes() - 1);
		try{
		    m_Filter.setInputFormat(instances);
		    Instances filtered = Filter.useFilter(instances, m_Filter);
		    trainingData = filtered;
		    // train classifier on training data
		    m_Classifier.buildClassifier(instances);
		    
		    return m_Classifier;
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
			return null;
		}
	}
	 
	 /**
	  * @param a State of our agent
	  * @param o State of opponent agent
	  * @return An instance containing the current state of the game.
	  */
	 private Instance createInstanceToPredict(StateAgent a, StateAgent o)
	 {
		 double[] values = new double[trainingData.numAttributes()];
		 values[0] = a.getCol();
         values[1] = a.getRow();
         values[2] = a.getHealthPoints();
         values[3] = a.getStaminaPoints();
         values[4] = o.getCol();
         values[5] = o.getRow();
         values[6] = o.getHealthPoints();
         values[7] = o.getStaminaPoints();
         
         Instance i = new Instance( 1.0, values.clone());
         i.setDataset(trainingData);
         return i;
	 }
	 
	 public String toString() {
		    StringBuffer        result;

		    result = new StringBuffer();
		    result.append("Weka - Demo\n===========\n\n");

		    result.append("Classifier...: " 
		        + m_Classifier.getClass().getName() + " " 
		        + Utils.joinOptions(m_Classifier.getOptions()) + "\n");
		    if (m_Filter instanceof OptionHandler)
		      result.append("Filter.......: " 
		          + m_Filter.getClass().getName() + " " 
		          + Utils.joinOptions(((OptionHandler) m_Filter).getOptions()) + "\n");
		    else
		      result.append("Filter.......: "
		          + m_Filter.getClass().getName() + "\n");
		    result.append("\n");

		    result.append(m_Classifier.toString() + "\n");
		    
		    return result.toString();
		  }

     //Evaluate the gain in the next state an agent against the other agent.
     public int evalMove(StateAgent aCurrent, StateAgent aNext, StateAgent oCurrent, StateAgent oNext) {

         //Good gains.
         int WIN  = 30; //Winning is very very good.
         int HEALTH_WEIGHT = 15; //Health delta is good since we pretty much always want to have more stamina then the opponent.
         int STAMINA_WEIGHT = 8; //Stamina delta is good as well since we have more stamina then the opponent.
         int DISTANCE_WEIGHT = 5; //We want to get closer to the enemy.
         int MOVE_WEIGHT = 0; //Aggregation of state change points, with 0 being a pretty neutral move.

         //Get deltas in variable changes for agent A.
         int aDeltaHealth = aNext.getHealthPoints()  - aCurrent.getHealthPoints();
         int aDeltaStamina = aNext.getStaminaPoints() - aCurrent.getStaminaPoints();

         //Get deltas in variable changes for the opponent of the agent being evaluated.
         int oDeltaHealth = oNext.getHealthPoints()  - oCurrent.getHealthPoints();
         int oDeltaStamina = oNext.getStaminaPoints() - oCurrent.getStaminaPoints();

         //Check if we're moving closer or further away.
         int distanceGain = (Math.abs( aCurrent.getCol() - oCurrent.getCol() ) + Math.abs( aCurrent.getRow() - oCurrent.getRow() )) -
                            (Math.abs( aNext.getCol() - oNext.getCol() ) + Math.abs( aNext.getRow() - oNext.getRow() )) ;

         //TODO: CREATE A BETTER PLAYING STRATEGY.
         if(oNext.getHealthPoints() <= 0) return WIN;     //Death of the enemy is a win for us.
         //if(aNext.getHealthPoints() <= 0) return Integer.MIN_VALUE;   //We don't want to die.
         //Aggregate gains in move.
         if(aDeltaHealth > oDeltaHealth) MOVE_WEIGHT += HEALTH_WEIGHT;
         //if(aDeltaHealth < oDeltaHealth) MOVE_WEIGHT -= HEALTH_WEIGHT;
         if(aDeltaStamina > oDeltaStamina) MOVE_WEIGHT += STAMINA_WEIGHT;
         //if(aDeltaStamina < oDeltaStamina) MOVE_WEIGHT -= STAMINA_WEIGHT;
         if(distanceGain < 0) MOVE_WEIGHT += DISTANCE_WEIGHT;
         //if(distanceGain > 0) MOVE_WEIGHT -= DISTANCE_WEIGHT;

         return MOVE_WEIGHT;

     }

    public Card getNextMove(StateBattle stateBattle, ArrayList<Card> cards, Card cardPredicted) {

        MonteCarloResults res = monteCarlo(stateBattle, cards, cardPredicted, 0);
        return res.getCard();
    }

    private MonteCarloResults monteCarlo(StateBattle stateBattle, ArrayList<Card> cards, Card cardPredicted, int depth)  {

           Card cardToPlay = new CardRest();
           int score = Integer.MIN_VALUE;
           //System.out.println("Predicted card is: " + cardPredicted.toString());
           //TODO: Go deeper.
           for(Card card : cards) {

               Card[] moves = new Card[2];
               moves[m_noOpponentAgent] = cardPredicted;
               moves[m_noThisAgent] = card;

               StateBattle clone = (StateBattle)stateBattle.clone();

               StateAgent usBefore = (StateAgent)clone.getAgentState(m_noThisAgent).clone();
               StateAgent themBefore = (StateAgent)clone.getAgentState(m_noOpponentAgent).clone();
               clone.play(moves);

               StateAgent usAfter = (StateAgent)clone.getAgentState(m_noThisAgent).clone();
               StateAgent themAfter = (StateAgent)clone.getAgentState(m_noOpponentAgent).clone();

               int eval = evalMove(usBefore, usAfter, themBefore, themAfter);

               if(depth <  TREE_DEPTH) {

                   //Create an instance using the current state of the board.
                   Instance nextStateInstance = createInstanceToPredict(usAfter, themAfter);

                   Card nextCardPredicted = null;
                   try{
                       double prediction = m_Classifier.classifyInstance(nextStateInstance);
                       String cardName = trainingData.classAttribute().value((int) prediction);
                       nextCardPredicted = cardsMap.get(cardName);
                   }
                   catch(Exception e)
                   {
                       System.out.println("Classify error: " + e.getMessage());
                   }

                   //Populate the card array with cards we have enough stamina to use.
                   ArrayList<Card> nextCards = m_deck.getCards( usAfter.getStaminaPoints() );
                   MonteCarloResults future = monteCarlo(clone, nextCards, nextCardPredicted, depth + 1);
                   eval += future.getScore();
               }

               if(eval > score) {

                   score = eval;
                   cardToPlay = card;
               }
           }
           //if(depth == 0) System.out.println("We select:  " + cardToPlay + "with score " + score + " against " +cardPredicted.toString());
           return new MonteCarloResults(score, cardToPlay);
    }
}