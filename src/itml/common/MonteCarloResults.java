package itml.common;
import itml.cards.Card;

/**
 * Created with IntelliJ IDEA.
 * User: vidirr
 * Date: 9/22/13
 * Time: 6:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class MonteCarloResults {
    private int score;
    private Card card;

    public MonteCarloResults() {
        score = Integer.MIN_VALUE;
        card = null;
    }

    public MonteCarloResults(int score, Card card){
        this.score = score;
        this.card = card;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
