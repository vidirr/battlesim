package itml.common;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.trees.J48;
import weka.core.Utils;

public class Classifiers {
    private static String[] classifier_Options = {"-U"};
    
    public static Classifier J48()
    {
    	Classifier c = new J48();
    	try{
    		c.setOptions(classifier_Options);
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    	}
    	return c;
    }
    
    public static Classifier NaiveBayes()
    {
    	NaiveBayes c = new NaiveBayes();
    	try{
    		c.setUseSupervisedDiscretization(true);
    	}
    	catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    	}
    	return c;
    }
    
    public static Classifier MultilayerPerceptron(){
    	Classifier c = new weka.classifiers.functions.MultilayerPerceptron();

        try {
        	//Default options from weka docs
            c.setOptions(Utils.splitOptions("-L 0.3 -M 0.2 -N 500 -V 0 -S 0 -E 20 -H a -R"));
        }
        catch(Exception e)
    	{
    		System.out.println(e.getMessage());
    	}
    	return c;
    }
}
