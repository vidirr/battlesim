package itml.common;

import weka.core.OptionHandler;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.Randomize;

public class Filters {
	 private static String[] filterOptions = {};
	 
	 public static Filter RandomizeFilter(){
		 Filter f = new Randomize();
		 try{
			 if (f instanceof OptionHandler)
			 	((OptionHandler) f).setOptions(filterOptions);
		 }
		 catch(Exception e){
			 System.out.println(e.getMessage());
		 }
		 return f;
	 }
}
